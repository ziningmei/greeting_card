package com.card.controller;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.card.dao.CardDao;
import com.card.global.GlobalContract;
import com.card.util.Base64Util;
import com.card.util.WXShareUtils;
import com.card.vo.CardVO;
import com.card.vo.WxVO;

import net.sf.json.JSONObject;

@Controller
@RequestMapping(value = "wx")
public class WXShareAction {

	@Resource
	private CardDao cardDao;

	/**
	 * 分享验证
	 * 
	 * @author jq
	 * @time 2016年1月22日 下午9:52:11
	 * @param session
	 * @param request
	 * @param wxVO
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/getSign", method = RequestMethod.POST)
	public String getSignPackage(HttpSession session,
			HttpServletRequest request, @RequestBody WxVO wxVO) {
		Properties properties = new Properties();
		WxVO rvo = new WxVO();
		try {
			properties.load(WXShareUtils.class.getClassLoader()
					.getResourceAsStream("wx.properties"));
			String appid = properties.getProperty("wx.appid");
			Map<String, String> map = WXShareUtils.sign(wxVO.getUrl());
			rvo.setNonceStr(map.get("nonceStr"));
			rvo.setTimestamp(map.get("timestamp"));
			rvo.setSignature(map.get("signature"));
			rvo.setAppid(appid);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(rvo.toString());
		return JSONObject.fromObject(rvo).toString();
	}

	@ResponseBody
	@RequestMapping(value = "/insertCard", method = RequestMethod.POST)
	public String insertCard(HttpSession session, HttpServletRequest request,
			@RequestBody CardVO cardVO) {
		CardVO rvo = new CardVO();
		try {
			if (!cardVO.getImg1().equals("")) {
				// 图片保存路径
				String img_base64=cardVO.getImg1();
				String filePath = GlobalContract.LOCAL_URL;
				File dir = new File(filePath);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				String fileFormat = ".".concat(img_base64.substring(11,
						img_base64.indexOf(";")));
				String fileData = img_base64.substring(img_base64.indexOf(",") + 1);
				String fileName = Base64Util.getfileName().concat(fileFormat);
				Base64Util.decodeFile(fileData, filePath.concat(fileName));
				cardVO.setImg1(fileName);
			}
			if (!cardVO.getImg2().equals("")) {
				// 图片保存路径
				String img_base64=cardVO.getImg2();
				String filePath = GlobalContract.LOCAL_URL;
				File dir = new File(filePath);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				String fileFormat = ".".concat(img_base64.substring(11,
						img_base64.indexOf(";")));
				String fileData = img_base64.substring(img_base64.indexOf(",") + 1);
				String fileName = Base64Util.getfileName().concat(fileFormat);
				Base64Util.decodeFile(fileData, filePath.concat(fileName));
				cardVO.setImg2(fileName);
			}
			cardDao.insertCard(cardVO);
			rvo.setId(cardVO.getId());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return JSONObject.fromObject(rvo).toString();
	}

	@ResponseBody
	@RequestMapping(value = "/getCard", method = RequestMethod.POST)
	public String getCard(HttpSession session, HttpServletRequest request,
			@RequestBody CardVO cardVO) {
		CardVO rvo = new CardVO();
		try {
			rvo = cardDao.getCard(cardVO);
			rvo.setImg1(GlobalContract.APACHE_URL.concat(rvo.getImg1()));
			rvo.setImg2(GlobalContract.APACHE_URL.concat(rvo.getImg2()));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println(rvo.toString());
		return JSONObject.fromObject(rvo).toString();
	}
}
