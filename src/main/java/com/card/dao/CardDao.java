/**
 * @author jq
 * 2016年1月24日 下午9:49:43
 */
/**
 * @author jq
 *
 */
package com.card.dao;

import java.sql.SQLException;

import com.card.vo.CardVO;


public interface CardDao{
	
	int insertCard(CardVO cardVO) throws SQLException;

	CardVO getCard(CardVO cardVO)throws SQLException;
}