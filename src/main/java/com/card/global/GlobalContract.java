package com.card.global;

/**
 * 静态变量定义
 * 
 * @author czt
 * @time 2015年5月20日 上午11:12:30
 */
public class GlobalContract {

	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 文件服务器 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */
	/** 文件服务器 —— 地址 */
//	public static final String APACHE_URL = "http://localhost:80/img/";
	public static final String APACHE_URL = "http://120.27.105.153:8080/img/";
	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 本地文件存储路径
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */
	/** 本地 文件保存路径 */
	//public static final String LOCAL_URL = "E:/php/phpStudy/WWW/img/";
	// public static final String LOCAL_URL = "/Users/ziningmei/img/";
	public static final String LOCAL_URL = "E:/WWW/img/";
	/** 用户头像 图片后缀 */
	public static final String USER_IMG_END = ".jpg";

	/** 计数，满999，还原100，重新 +1 FileOptionUtil.class */
	public static long COUNT = 100;

	/** 日志开始 */
	public static final String LOG_BEGIN = "begin";
	/** 日志结束 */
	public static final String LOG_END = "end";
}
