package com.card.vo;

public class WxVO {
	
	private String url;
	private String nonceStr;
	private String timestamp;
	private String signature;
	
	private String appid;

	@Override
	public String toString() {
		return "WxVO [url=" + url + ", nonceStr=" + nonceStr + ", timestamp="
				+ timestamp + ", signature=" + signature + ", appid=" + appid
				+ "]";
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}
	
	
}
