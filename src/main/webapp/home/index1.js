/**
 * 
 */
(function($) {

	var index = function() {

		return {

			/**
			 * 默认参数
			 */
			defaultOption: {
				err_tip: "",
				img_flag:1,
				img1:"",
				img2:"",
				id:0,
			},
			/**
			 * 初始化
			 */
			init: function() {
				// index.initInformation();
				
				index.initInformation();
				index.init_click();
			},
			initInformation: function() {
				maskUtil.showMask("main");
				var obj = new Object();
				obj.id=Number(index.getQueryString("id"));
				obj.url = window.location.href;
				var url = "wx/getCard.do";
				commonAjax.ajaxPost(url, obj, function(result) {
					if(result.img1!=""){
						$("#img1").attr("src",result.img1);
					}
					if(result.img2!="http://120.27.105.153:8080/img/"){
						$("#img2").attr("src",result.img2);
					}
					$("#i_message").html(result.message);
					index.wxconfig();
					maskUtil.hideMask("main");
				});
			},
			getQueryString:function(name) {
				var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
				var r = window.location.search.substr(1).match(reg);
				if (r != null) 
					return unescape(r[2]); 
				return '';
			},
			wxconfig: function() {
				shareTitle = "新年到，快点收下我的新年祝福吧！";
				shareContent = "据说里面有的不仅是祝福哦！还有一个大红包哦！";
				currentUrl = "http://ziningmei.ren/greeting_card/direct.html?id="+index.getQueryString("id")+"&page="+index.getQueryString("page");
				shareImageUrl = "http://ziningmei.ren/greeting_card/img/shareimg.png";
				var obj = new Object();
				obj.url = window.location.href;
				var url = "wx/getSign.do";
				commonAjax.ajaxPost(url, obj, function(result) {
					var data = result;
					wx.config({
						appId: data.appid,
						timestamp: data.timestamp,
						nonceStr: data.nonceStr,
						signature: data.signature,
						jsApiList: [ // 需要使用的网页服务接口
							'checkJsApi', // 判断当前客户端版本是否支持指定JS接口
							'onMenuShareTimeline', // 分享给好友
							'onMenuShareAppMessage', // 分享到朋友圈
							'onMenuShareQQ', // 分享到QQ
							'onMenuShareWeibo' // 分享到微博
						]
					});
					wx.ready(function() {
						wx.onMenuShareAppMessage({
							title: shareTitle,
							desc: shareContent,
							link: currentUrl,
							imgUrl: shareImageUrl
						});

						wx.onMenuShareTimeline({
							title: shareTitle,
							desc: shareContent,
							link: currentUrl,
							imgUrl: shareImageUrl
						});
					});

					wx.error(function(res) {
						// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
					});
				});
			},
			init_click: function() {
				flag = 0;
				$("#share").click(function(){
					$("#qrcodeShare").show();
					$("#qrcodeShare").click(function(){
						$("#qrcode").hide();
					});
				});
				$("#zhizuo").click(function(){
					window.location.href="index.html";
				});
				$("#guanzhu").click(function(){
					$("#qrcode").show();
					$("#qrcode").click(function(){
						$("#qrcode").hide();
					});
				});
			},
			click_uploadImg:function (){
				var dataurl = $('#preview').cropper('getCroppedCanvas').toDataURL('image/png');
				//index.defaultOption.img1=dataurl;
				switch (index.defaultOption.img_flag) {
				case 1:
					$("#img1").attr("src",dataurl);
					index.defaultOption.img1=dataurl;
					break;
				case 2:
					$("#img2").attr("src",dataurl);
					index.defaultOption.img2=dataurl;
					break;
				}
				$("#imageModal").modal("hide");
				$("#btn_chooseImg").show();
//				var obj=new Object();
//				obj.imgUrl=dataurl;
//				var url="wx/uploadImg.do";
//				commonAjax.ajaxPost(url, obj, function(result){
//					alert(result.imgUrl);
//					$("#imageModal").modal("hide");
//				});
			},
			/**
			 * 初始化cropper插件
			 * 
			 * @returns
			 */
			initCropper : function() {
				$('#preview').cropper({
					aspectRatio : 2 / 3,
					//autoCropArea : 0.65,
					movable : false,
					zoomable : false,
					rotatable : false,
					scalable : false
				});
			},
			validate : function() {
				if ($("#img1").attr("src") == "img/man.png"
						|| $("#img").attr("src") == "img/woman.png") {
					alert("头像未上传");
				} else if ($("#i_message").html() == "") {
					alert("文字不能为空");
				} else {
					return true;
				}
			},
			fileSelectHandler:function () {
				var oFile = $('#input_File')[0].files[0];
				$("#input_File").val('');
				var rFilter = /^(image\/jpeg|image\/png|image\/jpg)$/i;
				if (!rFilter.test(oFile.type)) {
					alert('请选择jpg、jpeg或png格式的图片');
					return;
				}
				// check for file size
				if (oFile.size > 4096 * 1024) {
					alert("图片太大");
					return;
				}
				// preview element
				var oImage = document.getElementById('preview');
				// prepare HTML5 FileReader
				var oReader = new FileReader();
				// Create variables (in this scope) to hold the Jcrop API and image size
				var jcrop_api, boundx, boundy;
				oReader.onload = function(e) {
					$("#btn_chooseImg").hide();
					// e.target.result contains the DataURL which we can use as a source of the image
					oImage.src = e.target.result;
					index.initCropper();
					/*oImage.onload = function() { // onload event handler
						// display step 2
						$('.step2').fadeIn(500);
						// display some basic image info
						var sResultFileSize = index.bytesToSize(oFile.size);
						// destroy Jcrop if it is existed
						if (typeof jcrop_api != 'undefined'){
							jcrop_api.destroy();
						}
						// initialize Jcrop
						$('#preview').Jcrop({
							minSize: [64, 96], // min crop size
							aspectRatio: 2/3, // keep aspect ratio 1:1
							bgFade: true, // use fade effect
							bgOpacity: .3, // fade opacity
							onChange: index.updateInfo,
							onSelect: index.updateInfo,
							onRelease: index.clearInfo,
							autoCropArea: 0.65,
						}, function() {
							// use the Jcrop API to get the real image size
							var bounds = this.getBounds();
							boundx = bounds[0];
							boundy = bounds[1];
							// Store the Jcrop API in the jcrop_api variable
							jcrop_api = this;
						});
					};*/
				};
				// read selected file as DataURL
				oReader.readAsDataURL(oFile);
			},
		}
	}();
	window.index = index;
})(jQuery);